class CreateSupplierProfiles < ActiveRecord::Migration
  def change
    create_table :supplier_profiles do |t|
    	t.string "supplier_name"
    	t.string "product_name"
    	t.string "quantity"
    	t.string "price"
    	t.timestamps null: false
    end
  end
end
