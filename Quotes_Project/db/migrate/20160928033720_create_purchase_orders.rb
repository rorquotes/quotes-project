class CreatePurchaseOrders < ActiveRecord::Migration
  def change
    create_table :purchase_orders do |t|
	t.references "product_name"
	t.string "supplier_name"
	t.integer "quantity"
      t.timestamps null: false
    end
  end
end
