class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products, :id =>false do |t|
    	t.string "product_name"
    	t.string "quotation"
      t.timestamps null: false
    end
  end
end
