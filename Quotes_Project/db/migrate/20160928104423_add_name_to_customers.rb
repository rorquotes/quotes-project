class AddNameToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :name, :string
    add_column :customers, :phone_no, :bigint
    add_column :customers, :address, :string
    add_column :customers, :state, :string
  end
end
